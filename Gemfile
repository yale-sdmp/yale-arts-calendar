# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.3'

gem 'bootsnap', '~> 1.10.3', require: false
gem 'cssbundling-rails', '~> 1.0.0'
gem 'devise', '~> 4.8'
gem 'devise_cas_authenticatable', '~> 2.0'
gem 'dotenv-rails', '~> 2.7'
gem 'jbuilder', '~> 2.11.5'
gem 'jsbundling-rails', '~> 1.0.0'
gem 'pg', '~> 1.1'
gem 'puma', '~> 5.0'
gem 'pundit', '~> 2.2'
gem 'rails', '~> 7.0.1'
gem 'sprockets-rails', '~> 3.4.2'
gem 'stimulus-rails', '~> 1.0.2'
gem 'turbo-rails', '~> 1.0.1'

group :development, :test do
  gem 'bundler-audit', '~> 0.9.0'
  gem 'capybara', '~> 3.36'
  gem 'debug', '~> 1.4'
  gem 'factory_bot_rails', '~> 6.2'
  gem 'ffaker', '~> 2.20'
  gem 'foreman', '~> 0.87.2'
  gem 'rspec-rails', '~> 5.1'
  gem 'rubocop', '~> 1.25'
  gem 'rubocop-rails', '~> 2.13'
  gem 'rubocop-rspec', '~> 2.8'
  gem 'selenium-webdriver', '~> 4.1'
  gem 'shoulda-matchers', '~> 5.1'
  gem 'webmock', '~> 3.14'
end

group :development do
  gem 'web-console', '~> 4.2'
end

gem 'tzinfo-data', platforms: %i(mingw mswin x64_mingw jruby)
