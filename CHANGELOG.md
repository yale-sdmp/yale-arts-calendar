# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Added

* Added the initial rails scaffold ([1](https://gitlab.com/yale-sdmp/yale-arts-calendar/-/issues/1)).
* Added dev dependencies ([2](https://gitlab.com/yale-sdmp/yale-arts-calendar/-/issues/2)).
* Added testing pipelines and dev dockerfile ([3](https://gitlab.com/yale-sdmp/yale-arts-calendar/-/issues/3)).
* Added prod dockerfile ([4](https://gitlab.com/yale-sdmp/yale-arts-calendar/-/issues/4)).
* Added authenticat/authorization ([5](https://gitlab.com/yale-sdmp/yale-arts-calendar/-/issues/5)).
* Added yale bootstrap overrides ([9](https://gitlab.com/yale-sdmp/yale-arts-calendar/-/issues/9)).
