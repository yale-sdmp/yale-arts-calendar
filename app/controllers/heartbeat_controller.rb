# frozen_string_literal: true

# Controller for the heartbeat
class HeartbeatController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    render plain: 'Application is up.'
  end
end
