# frozen_string_literal: true

# Class for users
class User < ApplicationRecord
  devise :cas_authenticatable

  validates :username, presence: true, uniqueness: true
end
