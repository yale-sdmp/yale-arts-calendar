# frozen_string_literal: true

# Factory for creating users
FactoryBot.define do
  factory :user do
    username { "#{('aa'..'zz').to_a.sample}#{rand(100..999)}" }
  end
end
