# frozen_string_literal: true

require 'selenium-webdriver'

Capybara.register_driver :selenium_chrome_headless_docker_friendly do |app|
  Capybara::Selenium::Driver.load_selenium
  browser_options = Selenium::WebDriver::Chrome::Options.new
  browser_options.add_argument('--headless')
  browser_options.add_argument('--disable-gpu')

  # Sandbox cannot be used inside unprivileged Docker container
  browser_options.add_argument('--no-sandbox')
  browser_options.add_argument('--disable-dev-shm-usage')
  Capybara::Selenium::Driver
    .new(app, browser: :chrome, options: browser_options)
end

Capybara.server = :puma, { Silent: true }
Capybara.javascript_driver = :selenium_chrome_headless_docker_friendly
