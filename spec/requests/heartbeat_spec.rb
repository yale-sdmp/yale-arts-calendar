# frozen_string_literal: true

# Tests for the heartbeat controller
RSpec.describe 'Heartbeat' do
  before do
    get heartbeat_index_path
  end

  it 'returns ok when the app is up' do
    expect(response).to have_http_status(:ok)
  end

  it 'has the correct payload' do
    expect(response.body).to include('Application is up')
  end
end
