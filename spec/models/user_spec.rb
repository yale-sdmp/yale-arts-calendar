# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  context 'with basic validations' do
    subject! { build(:user) }

    it { is_expected.to validate_presence_of(:username) }
    it { is_expected.to validate_uniqueness_of(:username) }
  end
end
