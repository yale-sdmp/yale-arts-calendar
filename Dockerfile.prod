# Stage 1: Set up a base for the next stages
FROM ruby:3.0.3-alpine AS base

RUN apk update \
  && apk upgrade \
  && apk add --update --no-cache nodejs postgresql-dev tzdata

WORKDIR /app

ENV RAILS_ENV=production \
    BUNDLE_PATH=vendor/bundle \
    BUNDLE_WITHOUT=development:test


# Stage 2: Install rails dependencies
FROM base AS gems

RUN apk add --no-cache build-base

# Install gems
COPY Gemfile* ./
RUN bundle install --jobs $(nproc)


# Stage 3: Precompile assets
FROM base AS assets

RUN apk add --no-cache yarn

# Install javascript dependencies
COPY package.json yarn.lock ./
RUN yarn install --production --check-files

# Copy over the gems installed from the previous stage
COPY --from=gems /app/vendor ./vendor
COPY . ./
RUN bundle exec rails assets:precompile


# Stage 4: Take prebuilt assets and run in a fresh alpine container
FROM base AS app

# Copy artifacts from previous stages
COPY --from=gems /app/vendor ./vendor
COPY --from=assets /app/public ./public
COPY . ./

ENTRYPOINT ["./docker-entrypoint.sh"]

CMD ["bundle", "exec", "rails", "s", "-b", "0.0.0.0"]
